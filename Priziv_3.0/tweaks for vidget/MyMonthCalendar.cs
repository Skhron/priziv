﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Priziv_3._0.tweaks_for_vidget
{
    public class MyMonthCalendar : MonthCalendar
    {
        public  int dblcount = 0;
        public  DateTime dbltmp = new DateTime();

        [DllImport("uxtheme.dll", ExactSpelling = true, CharSet = CharSet.Unicode)]
        static extern int SetWindowTheme(IntPtr hwnd, string pszSubAppName, string pszSubIdList);
        protected override void OnHandleCreated(EventArgs e)
        {
            SetWindowTheme(Handle, string.Empty, string.Empty);
            base.OnHandleCreated(e);
        }
    }
}
