﻿using Priziv_3._0.tweaks_for_vidget;
using System.Drawing;

namespace Priziv_3._0
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.Recruit = new System.Windows.Forms.ToolStripMenuItem();
            this.Find = new System.Windows.Forms.ToolStripMenuItem();
            this.Directive = new System.Windows.Forms.ToolStripMenuItem();
            this.Service = new System.Windows.Forms.ToolStripMenuItem();
            this.Daily_report = new System.Windows.Forms.ToolStripMenuItem();
            this.Weekly_report = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.Duty = new System.Windows.Forms.ToolStripMenuItem();
            this.List = new System.Windows.Forms.ToolStripMenuItem();
            this.Stats = new System.Windows.Forms.ToolStripMenuItem();
            this.military_type = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.Education = new System.Windows.Forms.ToolStripMenuItem();
            this.by_name = new System.Windows.Forms.ToolStripMenuItem();
            this.high_F = new System.Windows.Forms.ToolStripMenuItem();
            this.high_wip_F = new System.Windows.Forms.ToolStripMenuItem();
            this.Mid_pro_F = new System.Windows.Forms.ToolStripMenuItem();
            this.Mid_spec_F = new System.Windows.Forms.ToolStripMenuItem();
            this.Mid_F = new System.Windows.Forms.ToolStripMenuItem();
            this.Mid_wip_F = new System.Windows.Forms.ToolStripMenuItem();
            this.nine_F = new System.Windows.Forms.ToolStripMenuItem();
            this.by_office = new System.Windows.Forms.ToolStripMenuItem();
            this.high_o = new System.Windows.Forms.ToolStripMenuItem();
            this.high_wip_o = new System.Windows.Forms.ToolStripMenuItem();
            this.Mid_pro_o = new System.Windows.Forms.ToolStripMenuItem();
            this.Mid_spec_o = new System.Windows.Forms.ToolStripMenuItem();
            this.Mid = new System.Windows.Forms.ToolStripMenuItem();
            this.Mid_wip_o = new System.Windows.Forms.ToolStripMenuItem();
            this.nine_o = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.Come_date = new System.Windows.Forms.ToolStripMenuItem();
            this.send_date = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.yearly_date = new System.Windows.Forms.ToolStripMenuItem();
            this.Documents = new System.Windows.Forms.ToolStripMenuItem();
            this.Book_com = new System.Windows.Forms.ToolStripMenuItem();
            this.Generate_lists = new System.Windows.Forms.ToolStripMenuItem();
            this.Generate_lists_all = new System.Windows.Forms.ToolStripMenuItem();
            this.Generate_lists_idea = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.Vipa_D = new System.Windows.Forms.ToolStripMenuItem();
            this.Vipa_P = new System.Windows.Forms.ToolStripMenuItem();
            this.DB = new System.Windows.Forms.ToolStripMenuItem();
            this.Table_opt = new System.Windows.Forms.ToolStripMenuItem();
            this.Table_check = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.Create_DB = new System.Windows.Forms.ToolStripMenuItem();
            this.System = new System.Windows.Forms.ToolStripMenuItem();
            this.program_settings = new System.Windows.Forms.ToolStripMenuItem();
            this.Print_settings = new System.Windows.Forms.ToolStripMenuItem();
            this.Logs = new System.Windows.Forms.ToolStripMenuItem();
            this.Users = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.myMonthCalendar1 = new Priziv_3._0.tweaks_for_vidget.MyMonthCalendar();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Recruit,
            this.Service,
            this.Stats,
            this.Documents,
            this.DB,
            this.System});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(558, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "MainMenu";
            // 
            // Recruit
            // 
            this.Recruit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Find,
            this.Directive});
            this.Recruit.Name = "Recruit";
            this.Recruit.Size = new System.Drawing.Size(82, 20);
            this.Recruit.Text = "Призывник";
            // 
            // Find
            // 
            this.Find.Name = "Find";
            this.Find.Size = new System.Drawing.Size(135, 22);
            this.Find.Text = "Найти";
            // 
            // Directive
            // 
            this.Directive.Name = "Directive";
            this.Directive.Size = new System.Drawing.Size(135, 22);
            this.Directive.Text = "Директивы";
            // 
            // Service
            // 
            this.Service.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Daily_report,
            this.Weekly_report,
            this.toolStripSeparator1,
            this.Duty,
            this.List});
            this.Service.Name = "Service";
            this.Service.Size = new System.Drawing.Size(59, 20);
            this.Service.Text = "Сервис";
            // 
            // Daily_report
            // 
            this.Daily_report.Name = "Daily_report";
            this.Daily_report.Size = new System.Drawing.Size(172, 22);
            this.Daily_report.Text = "Отчет за день";
            // 
            // Weekly_report
            // 
            this.Weekly_report.Name = "Weekly_report";
            this.Weekly_report.Size = new System.Drawing.Size(172, 22);
            this.Weekly_report.Text = "Отчет за неделю";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(169, 6);
            // 
            // Duty
            // 
            this.Duty.Name = "Duty";
            this.Duty.Size = new System.Drawing.Size(172, 22);
            this.Duty.Text = "Наряд";
            // 
            // List
            // 
            this.List.Name = "List";
            this.List.Size = new System.Drawing.Size(172, 22);
            this.List.Text = "Списки от компл.";
            // 
            // Stats
            // 
            this.Stats.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.military_type,
            this.toolStripSeparator2,
            this.Education,
            this.toolStripSeparator3,
            this.Come_date,
            this.send_date,
            this.toolStripSeparator4,
            this.yearly_date});
            this.Stats.Name = "Stats";
            this.Stats.Size = new System.Drawing.Size(80, 20);
            this.Stats.Text = "Статистика";
            // 
            // military_type
            // 
            this.military_type.Name = "military_type";
            this.military_type.Size = new System.Drawing.Size(179, 22);
            this.military_type.Text = "По роду войск";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(176, 6);
            // 
            // Education
            // 
            this.Education.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.by_name,
            this.by_office});
            this.Education.Name = "Education";
            this.Education.Size = new System.Drawing.Size(179, 22);
            this.Education.Text = "Образование";
            // 
            // by_name
            // 
            this.by_name.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.high_F,
            this.high_wip_F,
            this.Mid_pro_F,
            this.Mid_spec_F,
            this.Mid_F,
            this.Mid_wip_F,
            this.nine_F});
            this.by_name.Name = "by_name";
            this.by_name.Size = new System.Drawing.Size(158, 22);
            this.by_name.Text = "По фамильно";
            // 
            // high_F
            // 
            this.high_F.Name = "high_F";
            this.high_F.Size = new System.Drawing.Size(233, 22);
            this.high_F.Text = "высшее";
            // 
            // high_wip_F
            // 
            this.high_wip_F.Name = "high_wip_F";
            this.high_wip_F.Size = new System.Drawing.Size(233, 22);
            this.high_wip_F.Text = "Незаконченное высшее";
            // 
            // Mid_pro_F
            // 
            this.Mid_pro_F.Name = "Mid_pro_F";
            this.Mid_pro_F.Size = new System.Drawing.Size(233, 22);
            this.Mid_pro_F.Text = "Среднее-профессиональное";
            // 
            // Mid_spec_F
            // 
            this.Mid_spec_F.Name = "Mid_spec_F";
            this.Mid_spec_F.Size = new System.Drawing.Size(233, 22);
            this.Mid_spec_F.Text = "Среднее-специальное";
            // 
            // Mid_F
            // 
            this.Mid_F.Name = "Mid_F";
            this.Mid_F.Size = new System.Drawing.Size(233, 22);
            this.Mid_F.Text = "Среднее";
            // 
            // Mid_wip_F
            // 
            this.Mid_wip_F.Name = "Mid_wip_F";
            this.Mid_wip_F.Size = new System.Drawing.Size(233, 22);
            this.Mid_wip_F.Text = "Незаконченное среднее";
            // 
            // nine_F
            // 
            this.nine_F.Name = "nine_F";
            this.nine_F.Size = new System.Drawing.Size(233, 22);
            this.nine_F.Text = "9 классов и ниже";
            // 
            // by_office
            // 
            this.by_office.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.high_o,
            this.high_wip_o,
            this.Mid_pro_o,
            this.Mid_spec_o,
            this.Mid,
            this.Mid_wip_o,
            this.nine_o});
            this.by_office.Name = "by_office";
            this.by_office.Size = new System.Drawing.Size(158, 22);
            this.by_office.Text = "По военкомату";
            // 
            // high_o
            // 
            this.high_o.Name = "high_o";
            this.high_o.Size = new System.Drawing.Size(233, 22);
            this.high_o.Text = "Высшее";
            // 
            // high_wip_o
            // 
            this.high_wip_o.Name = "high_wip_o";
            this.high_wip_o.Size = new System.Drawing.Size(233, 22);
            this.high_wip_o.Text = "Незаконченное высшее";
            // 
            // Mid_pro_o
            // 
            this.Mid_pro_o.Name = "Mid_pro_o";
            this.Mid_pro_o.Size = new System.Drawing.Size(233, 22);
            this.Mid_pro_o.Text = "Среднее-профессиональное";
            // 
            // Mid_spec_o
            // 
            this.Mid_spec_o.Name = "Mid_spec_o";
            this.Mid_spec_o.Size = new System.Drawing.Size(233, 22);
            this.Mid_spec_o.Text = "Среднее-специальное";
            // 
            // Mid
            // 
            this.Mid.Name = "Mid";
            this.Mid.Size = new System.Drawing.Size(233, 22);
            this.Mid.Text = "Среднее";
            // 
            // Mid_wip_o
            // 
            this.Mid_wip_o.Name = "Mid_wip_o";
            this.Mid_wip_o.Size = new System.Drawing.Size(233, 22);
            this.Mid_wip_o.Text = "Незаконченное среднее";
            // 
            // nine_o
            // 
            this.nine_o.Name = "nine_o";
            this.nine_o.Size = new System.Drawing.Size(233, 22);
            this.nine_o.Text = "9 классов и ниже";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(176, 6);
            // 
            // Come_date
            // 
            this.Come_date.Name = "Come_date";
            this.Come_date.Size = new System.Drawing.Size(179, 22);
            this.Come_date.Text = "По датам привоза";
            // 
            // send_date
            // 
            this.send_date.Name = "send_date";
            this.send_date.Size = new System.Drawing.Size(179, 22);
            this.send_date.Text = "По датам отправки";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(176, 6);
            // 
            // yearly_date
            // 
            this.yearly_date.Name = "yearly_date";
            this.yearly_date.Size = new System.Drawing.Size(179, 22);
            this.yearly_date.Text = "По годам";
            // 
            // Documents
            // 
            this.Documents.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Book_com,
            this.Generate_lists,
            this.Generate_lists_all,
            this.Generate_lists_idea,
            this.toolStripSeparator5,
            this.Vipa_D,
            this.Vipa_P});
            this.Documents.Name = "Documents";
            this.Documents.Size = new System.Drawing.Size(82, 20);
            this.Documents.Text = "Документы";
            // 
            // Book_com
            // 
            this.Book_com.Name = "Book_com";
            this.Book_com.Size = new System.Drawing.Size(303, 22);
            this.Book_com.Text = "Книга учета команд";
            // 
            // Generate_lists
            // 
            this.Generate_lists.Name = "Generate_lists";
            this.Generate_lists.Size = new System.Drawing.Size(303, 22);
            this.Generate_lists.Text = "Генерировать списки";
            // 
            // Generate_lists_all
            // 
            this.Generate_lists_all.Name = "Generate_lists_all";
            this.Generate_lists_all.Size = new System.Drawing.Size(303, 22);
            this.Generate_lists_all.Text = "Генерировать списки в один файл";
            // 
            // Generate_lists_idea
            // 
            this.Generate_lists_idea.Name = "Generate_lists_idea";
            this.Generate_lists_idea.Size = new System.Drawing.Size(303, 22);
            this.Generate_lists_idea.Text = "Генерировать списки с предназначением";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(300, 6);
            // 
            // Vipa_D
            // 
            this.Vipa_D.Name = "Vipa_D";
            this.Vipa_D.Size = new System.Drawing.Size(303, 22);
            this.Vipa_D.Text = "Випа Д";
            // 
            // Vipa_P
            // 
            this.Vipa_P.Name = "Vipa_P";
            this.Vipa_P.Size = new System.Drawing.Size(303, 22);
            this.Vipa_P.Text = "Випа Р";
            // 
            // DB
            // 
            this.DB.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Table_opt,
            this.Table_check,
            this.toolStripSeparator6,
            this.Create_DB});
            this.DB.Name = "DB";
            this.DB.Size = new System.Drawing.Size(34, 20);
            this.DB.Text = "БД";
            // 
            // Table_opt
            // 
            this.Table_opt.Name = "Table_opt";
            this.Table_opt.Size = new System.Drawing.Size(191, 22);
            this.Table_opt.Text = "Оптимизация таблиц";
            // 
            // Table_check
            // 
            this.Table_check.Name = "Table_check";
            this.Table_check.Size = new System.Drawing.Size(191, 22);
            this.Table_check.Text = "Проверка таблиц";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(188, 6);
            // 
            // Create_DB
            // 
            this.Create_DB.Name = "Create_DB";
            this.Create_DB.Size = new System.Drawing.Size(191, 22);
            this.Create_DB.Text = "Создать базу данных";
            // 
            // System
            // 
            this.System.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.program_settings,
            this.Print_settings,
            this.Logs,
            this.Users});
            this.System.Name = "System";
            this.System.Size = new System.Drawing.Size(66, 20);
            this.System.Text = "Система";
            // 
            // program_settings
            // 
            this.program_settings.Name = "program_settings";
            this.program_settings.Size = new System.Drawing.Size(204, 22);
            this.program_settings.Text = "Настройки программы";
            // 
            // Print_settings
            // 
            this.Print_settings.Name = "Print_settings";
            this.Print_settings.Size = new System.Drawing.Size(204, 22);
            this.Print_settings.Text = "Настройки печати";
            // 
            // Logs
            // 
            this.Logs.Name = "Logs";
            this.Logs.Size = new System.Drawing.Size(204, 22);
            this.Logs.Text = "Системные сообщения";
            // 
            // Users
            // 
            this.Users.Name = "Users";
            this.Users.Size = new System.Drawing.Size(204, 22);
            this.Users.Text = "Пользователи";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(346, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 86);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Текущая база";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "label1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboBox1);
            this.groupBox2.Location = new System.Drawing.Point(346, 119);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 63);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Выбор базы";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(37, 22);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 23);
            this.comboBox1.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Location = new System.Drawing.Point(346, 188);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 50);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Жетоны                Прод.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(101, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 15);
            this.label4.TabIndex = 1;
            this.label4.Text = "label4";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "label3";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Location = new System.Drawing.Point(346, 244);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(200, 100);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Пользователь";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "label2";
            // 
            // myMonthCalendar1
            // 
            this.myMonthCalendar1.Location = new System.Drawing.Point(29, 33);
            this.myMonthCalendar1.Name = "myMonthCalendar1";
            this.myMonthCalendar1.TabIndex = 6;
            this.myMonthCalendar1.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.myMonthCalendar1_DateSelected);
            // 
            // MainForm
            // 
            this.ClientSize = new System.Drawing.Size(558, 356);
            this.Controls.Add(this.myMonthCalendar1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Name = "MainForm";
            this.Text = "БАЗА";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem Recruit;
        private System.Windows.Forms.ToolStripMenuItem Find;
        private System.Windows.Forms.ToolStripMenuItem Directive;
        private System.Windows.Forms.ToolStripMenuItem Service;
        private System.Windows.Forms.ToolStripMenuItem Daily_report;
        private System.Windows.Forms.ToolStripMenuItem Weekly_report;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem Duty;
        private System.Windows.Forms.ToolStripMenuItem List;
        private System.Windows.Forms.ToolStripMenuItem Stats;
        private System.Windows.Forms.ToolStripMenuItem military_type;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem Education;
        private System.Windows.Forms.ToolStripMenuItem by_name;
        private System.Windows.Forms.ToolStripMenuItem high_F;
        private System.Windows.Forms.ToolStripMenuItem high_wip_F;
        private System.Windows.Forms.ToolStripMenuItem Mid_pro_F;
        private System.Windows.Forms.ToolStripMenuItem Mid_spec_F;
        private System.Windows.Forms.ToolStripMenuItem Mid_F;
        private System.Windows.Forms.ToolStripMenuItem Mid_wip_F;
        private System.Windows.Forms.ToolStripMenuItem nine_F;
        private System.Windows.Forms.ToolStripMenuItem by_office;
        private System.Windows.Forms.ToolStripMenuItem high_o;
        private System.Windows.Forms.ToolStripMenuItem high_wip_o;
        private System.Windows.Forms.ToolStripMenuItem Mid_pro_o;
        private System.Windows.Forms.ToolStripMenuItem Mid_spec_o;
        private System.Windows.Forms.ToolStripMenuItem Mid;
        private System.Windows.Forms.ToolStripMenuItem Mid_wip_o;
        private System.Windows.Forms.ToolStripMenuItem nine_o;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem Come_date;
        private System.Windows.Forms.ToolStripMenuItem send_date;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem yearly_date;
        private System.Windows.Forms.ToolStripMenuItem Documents;
        private System.Windows.Forms.ToolStripMenuItem Book_com;
        private System.Windows.Forms.ToolStripMenuItem Generate_lists;
        private System.Windows.Forms.ToolStripMenuItem Generate_lists_all;
        private System.Windows.Forms.ToolStripMenuItem Generate_lists_idea;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem Vipa_D;
        private System.Windows.Forms.ToolStripMenuItem Vipa_P;
        private System.Windows.Forms.ToolStripMenuItem DB;
        private System.Windows.Forms.ToolStripMenuItem Table_opt;
        private System.Windows.Forms.ToolStripMenuItem Table_check;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem Create_DB;
        private System.Windows.Forms.ToolStripMenuItem System;
        private System.Windows.Forms.ToolStripMenuItem program_settings;
        private System.Windows.Forms.ToolStripMenuItem Print_settings;
        private System.Windows.Forms.ToolStripMenuItem Logs;
        private System.Windows.Forms.ToolStripMenuItem Users;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label2;
        private MyMonthCalendar myMonthCalendar1;
    }
}

