﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Priziv_3._0
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            myMonthCalendar1.Font = new Font("Segoe UI", 16F, FontStyle.Regular, GraphicsUnit.Point);
        }

        private void myMonthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
            if (myMonthCalendar1.dblcount == 0)
            {
                myMonthCalendar1.dbltmp = e.Start.Date;
                myMonthCalendar1.dblcount++;
            }
            else
            {
                if (myMonthCalendar1.dbltmp == e.Start.Date)
                {
                    myMonthCalendar1.dblcount = 0;
                    //this.Hide();
                    CommandList form = new CommandList();
                    form.ShowDialog();
                    //string message = e.Start.Date.ToString();
                    //string caption = "Test";
                    // MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                    //DialogResult result;
                    //result = MessageBox.Show(message, caption, buttons);
                }
                else
                {
                    myMonthCalendar1.dbltmp = e.Start.Date;
                }
            }           
        }
    }
}
